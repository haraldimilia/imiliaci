# Nexus 3


- Lots of issues automating this with the default docker container


https://github.com/savoirfairelinux/ansible-nexus3-oss

https://support.sonatype.com/hc/en-us/articles/115002930827-Accessing-the-OrientDB-Console

https://support.sonatype.com/hc/en-us/articles/213467158-How-to-reset-a-forgotten-admin-password-in-Nexus-3-x


'
connect plocal:../sonatype-work/nexus3/db/security admin admin

update user SET password="$shiro1$SHA-512$1024$NE+wqQq/TmjZMvfI7ENh/g==$V4yPw8T64UQ6GfJfxYq2hLsVrBY8D1v+bktfOxGdt4b/9BthpWPNUy/CBk6V9iA0nHpzYzJFWO8v/tZFtES8CA==" UPSERT WHERE id="admin"

update user SET status="disabled" UPSERT WHERE id="anonymous"

update anonymous SET enabled="false" UPSERT WHERE id="anonymous"
'

bash-4.2$ pwd
/opt/sonatype/nexus

bash-4.2$ cat aaa |  java -jar ./lib/support/nexus-orient-console.jar

# https://www.youtube.com/watch?v=VBrEPBXuAVE