// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

if(!process.env.NEXUS_SERVER_URL) {
  console.error('NEXUS_SERVER_URL is not set');
  process.exit(-1);
}

if(!process.env.NEXUS_ADMIN_PASSWORD) {
  console.error('NEXUS_ADMIN_PASSWORD is not set');
  process.exit(-1);
}

var baseUrl = process.env.NEXUS_SERVER_URL.trim();
if(!baseUrl.endsWith('/')) {
  baseUrl = baseUrl + '/'
}

var browserName = 'chrome';
var chromeOptions =  {
  'args': [
     '--headless',
     '--disable-gpu',
     '--no-sandbox',
     '--remote-debugging-port=9222',
     '--window-size=1024,1080',
     '--disable-dev-shm-usage'
  ]
};

if(process.env.TEST_BROWSER) {
  browserName = process.env.TEST_BROWSER.trim().toLowerCase();
  chromeOptions = {
    'args': [
       '--sandbox',
       '--window-size=1024,1080',
    ]
  }
}

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  capabilities: {
    browserName: browserName,
    chromeOptions: chromeOptions
  },
  directConnect: true,
  baseUrl: baseUrl,
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};
