
import { Key } from 'protractor';

import { GitlabPage } from './pages/nexus.page';

const fs = require('fs');
const path = require('path');

describe('Enabling LDAP certificate', () => {
  const page = new GitlabPage();

  beforeAll(() => {
    page.navigateTo(true);
  });

  describe('login', () => {

    it('login as admin', async () => {
      await page.sleep(10 * 1000); // takes long time to load
      await page.waitUntilElementIsVisibledById('nx-header-signin-1143-btnEl');

      let signinButton = await page.getElementById('nx-header-signin-1143-btnEl');
      await page.safeClick(signinButton);

      const user_login = await page.getElementById('textfield-1165-inputEl');
      await user_login.clear();
      await user_login.sendKeys('admin');

      const user_password = await page.getElementById('textfield-1166-inputEl');
      await user_password.clear();
      await user_password.sendKeys(process.env.NEXUS_ADMIN_PASSWORD);

      await page.waitUntilElementIsEnabledById('button-1168-btnInnerEl');

      signinButton = await page.getElementById('button-1168-btnInnerEl');
      await page.safeClick(signinButton);

      await page.sleep(10 * 1000); // takes long time to load
    });

  });

  describe('configuring ldap', () => {

    it('goto new ldap manager', async () => {
      await page.gotoPage('/#admin/security/ldap');
      await page.waitUntilElementIsVisibledById('label-1149'); // LDAP Label
      await page.sleep(10 * 1000); // takes long time to load
    });

    it('goto IPA Entry', async () => {
      const ipaEntry = await page.getElementByXPath('//div[text()="IPA"]');
      await page.safeClick(ipaEntry);
    });

    it('open certificate dialog', async () => {
      // tslint:disable-next-line:max-line-length
      await page.waitUntilElementIsVisibledByXPath('//span[text()="View certificate"]'); // Save Button
      const button = await page.getElementByXPath('//span[text()="View certificate"]');
      await page.safeClick(button);

      // await page.waitUntilElementIsVisibledByXPath('//div[text()="Certificate Details"]'); // Dialog Label
      await page.sleep(10 * 1000); // takes long time to load

      // pice of crap
      await page.waitUntilElementIsEnabledByXPath('//span[text()="Add certificate to truststore"]'); // Save Button
      const truststore = await page.getElementByXPath('//span[text()="Add certificate to truststore"]');
      await page.safeClick(truststore);

      await page.sleep(10 * 1000); // takes long time to load
    });

    it('use certificate checkbox', async () => {
      // tslint:disable-next-line:max-line-length
      const checkbox = await page.getElementByXPath('//span[text()="Use certificates stored in the Nexus truststore to connect to external systems"]');
      await page.safeClick(checkbox);

      await page.sleep(10 * 1000); // takes long time to load
    });

    it('save ldap', async () => {
      await page.waitUntilElementIsEnabledByXPath('//span[text()="Save"]'); // Save Button

      const button = await page.getElementByXPath('//span[text()="Save"]');
      await page.safeClick(button);
      await page.sleep(10 * 1000); // takes long time to load
    });

  });
});
