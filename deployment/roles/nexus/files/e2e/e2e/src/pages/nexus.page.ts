import { BasePage } from './basepage.class';

export class GitlabPage extends BasePage {
  constructor() {
    super('/');
  }
}
