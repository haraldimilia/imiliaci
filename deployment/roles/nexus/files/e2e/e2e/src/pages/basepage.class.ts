import { browser, by, until, WebElement } from 'protractor';


export abstract class BasePage {
  static DEFAULT_WAIT_TIME = 5 * 1000;

  constructor(protected destination: string) {
    // https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular-on-page-load
    browser.waitForAngularEnabled(false);
  }

  async sleep(ms?: number): Promise<void> {
    if (ms == null || ms === undefined) {
      ms = 1000;
    }
    return await browser.driver.sleep(ms);
  }

  /**
   * please use this only for debugging - don't commit or a tslint rule will punish you :)
   */
  async debugSleepBrowser(): Promise<void> {
    return await this.sleep(30 * 1000);
  }

  /**
   * you might need to call this if you entered a text in a text input in order to trigger the value update
   */
  async blur(): Promise<{}> {
    // https://stackoverflow.com/questions/16488619/actions-through-webdriver-will-not-trigger-the-blur-event
    // https://stackoverflow.com/questions/497094/how-do-i-find-out-which-dom-element-has-the-focus
    return await browser.executeScript('if(document.activeElement){document.activeElement.blur();}');
  }

  async navigateTo(forceRefresh?: boolean): Promise<any> {
    const currentUrl = await browser.getCurrentUrl();
    const result = await browser.get(this.destination);
    if (forceRefresh) {
      // Todo: come up with a better solution
      // this is a bit clumsy
      // angular is configured in a way where we should not refresh pages
      // (which is in general a very good approach for single page applications)
      // for e2e we expect proper initalized pages
      if ( currentUrl && currentUrl.endsWith(this.destination)) {
        // tslint:disable-next-line:no-console
        console.warn('trigger full page reload', this.destination);
        // reload only if the urls is the same
        this.reloadPage();
      }
    } else {
      await this.sleep(500);
    }
    return await result;
  }

  async deleteAllCookies(): Promise<any> {
    // use this for a logout
    // https://stackoverflow.com/questions/35403614/clear-browser-cookies-with-selenium-webdriver-java-bindings
    await browser.manage().deleteAllCookies();
  }

  /**
   * this does a full page reload (a very costy operation - but you might need this)
   *
   * please consider using a navigateTo(true)
   *
   */
  async reloadPage(): Promise<any> {
    // use this for a logout
    // https://stackoverflow.com/questions/10245641/refreshing-web-page-by-webdriver-when-waiting-for-specific-condition

    // this all should work
    await browser.navigate().refresh();
    // await browser.get(await browser.getCurrentUrl());
    // await browser.navigate().to(await browser.getCurrentUrl());
    // await browser.findElement(by.id('Contact-us')).sendKeys(Key.F5);
    // await browser.executeScript('history.go(0)');
  }

  async gotoPage(subPage: string): Promise<any> {
    await browser.get(subPage);
  }

  // https://stackoverflow.com/questions/10245641/refreshing-web-page-by-webdriver-when-waiting-for-specific-condition

  async getTitle(): Promise<string> {
    return await browser.getTitle();
  }

  async getElementById(id: string, checkPresence?: boolean): Promise<WebElement> {
    if (checkPresence) {
      if ( !(await browser.isElementPresent(by.id(id)))) {
        return await null;
      }
    }
    return await browser.element(by.id(id));
  }

  async getElementByName(name: string, checkPresence?: boolean): Promise<WebElement> {
    if (checkPresence) {
      if ( !(browser.isElementPresent(by.name(name)))) {
        return await null;
      }
    }
    return await browser.element(by.name(name));
  }

  async getElementByXPath(xpath: string, checkPresence?: boolean): Promise<WebElement> {
    if (checkPresence) {
      if ( !(browser.isElementPresent(by.xpath(xpath)))) {
        return await null;
      }
    }
    return await browser.element(by.xpath(xpath));
  }

  async getElementByClassName(className: string, checkPresence?: boolean): Promise<WebElement> {
    if (checkPresence) {
      if ( !(browser.isElementPresent(by.className(className)))) {
        return await null;
      }
    }
    return await browser.element(by.className(className));
  }

  async getElementByCss(css: string, checkPresence?: boolean): Promise<WebElement> {
    if (checkPresence) {
      if ( !(browser.isElementPresent(by.css(css)))) {
        return await null;
      }
    }
    return await browser.element(by.css(css));
  }

  async safeClickById(id: string, checkPresence?: boolean): Promise<boolean> {
    const element = await this.getElementById(id, checkPresence);
    if (element == null || element === undefined) {
      return await false;
    }
    await this.safeClick(element);
    return await true;
  }


  async hasCssClass(element: WebElement, className: string): Promise<boolean> {
    const cssClasses = await element.getAttribute('class');
    return await cssClasses.indexOf(className) > -1;
  }

  async waitUntilElementIsVisibledById(id: string): Promise<WebElement> {
    const element = by.id(id);
    await browser.wait(until.elementLocated(element));
    const whatElement = browser.findElement(element);
    return await browser.wait(until.elementIsVisible(whatElement), BasePage.DEFAULT_WAIT_TIME);
  }

  async waitUntilElementIsVisibledByXPath(xpath: string): Promise<WebElement> {
    const element = by.xpath(xpath);
    await browser.wait(until.elementLocated(element));
    const whatElement = browser.findElement(element);
    return await browser.wait(until.elementIsVisible(whatElement), BasePage.DEFAULT_WAIT_TIME);
  }

  async waitUntilElementIsEnabledById(id: string): Promise<WebElement> {
    const element = by.id(id);
    await browser.wait(until.elementLocated(element));
    const whatElement = browser.findElement(element);
    return await browser.wait(until.elementIsEnabled(whatElement), BasePage.DEFAULT_WAIT_TIME);
  }

  async waitUntilElementIsEnabledByXPath(xpath: string): Promise<WebElement> {
    const element = by.xpath(xpath);
    await browser.wait(until.elementLocated(element));
    const whatElement = browser.findElement(element);
    return await browser.wait(until.elementIsEnabled(whatElement), BasePage.DEFAULT_WAIT_TIME);
  }

  /**
   * this ensures the visibility of the given element
   *
   * the call also makes sure that the element is not below the fixed header
   *
   * @param element element to make visible
   */
  async ensureVisible(element: WebElement): Promise<{}> {
    // idea from here https://stackoverflow.com/questions/3401343/scroll-element-into-view-with-selenium
    // https://stackoverflow.com/questions/13614112/using-scrollintoview-with-a-fixed-position-header
    // tslint:disable-next-line:max-line-length

    let c = '';
    c += '{';
    c += ' arguments[0].scrollIntoView(arguments[0].getBoundingClientRect()); ';
    c += '} ';

    return await browser.executeScript(c, element);
  }

  /**
   * use this method to click() any element
   * this makes sure that the element is visible in browser
   *
   * @param element element to click
   */
  async safeClick(element: WebElement): Promise<any> {
    await this.ensureVisible(element);
    await element.click();
    return await true;
  }

}
