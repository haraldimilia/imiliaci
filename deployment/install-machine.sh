#!/bin/bash

# arguments are zero based - so we check for 1 instead of 32
if [ "$#" -eq 1 ]; then
    echo "Illegal number of parameters"
    exit -1
fi

export HETZNER_API_TOKEN=$1
export MACHINENAME=$2

apt-get update
apt-get dist-upgrade -y --force-yes -qq
apt-get install ansible -y -qq
service docker restart
ansible-playbook ./playbook.yaml \
    --extra-vars "HETZNER_API_TOKEN=\"$HETZNER_API_TOKEN\" MACHINENAME=\"$MACHINENAME\"" 
