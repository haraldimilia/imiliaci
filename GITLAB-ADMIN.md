## login via webbrowser to your gitlab

- login as root / {{ gitlab_initial_root_password }}
- click on wrench
- click on settings "Sign-up restrictions"
- turn off "Sign-up enabled"
- Save Changes


# Turn "normal" users into Admin users with GitlabCE

- login via LDAP with user to read the Userinfo in Gitlab
- logout
- login as Gitlab root user
- click on wrench
- click on Users
- click on "Edit" next to the user that you want to make an admin
- set Access Level to "Admin"
- Save Changes