# How to setup CI / CD for gitlab

## ssh to ci host

- Become root
- Get the "SSH_PRIVATE_KEY" by "cat /home/gitlab-runner/.ssh/id_rsa"

## login via webbrowser to your gitlab

- login as root / {{ gitlab_initial_root_password }}
- create a new project e.g. "ExampeProject"
- click on "ExampleProject" / Settings / CI/CD / Secret variables / Expand
- Add - SSH_PRIVATE_KEY (the content you get with cat - carefull has multiple lines)
- Add - docker_registry / {{ gitlab_registry_domain_host_basename }}.{{ server_base_domain }}
- Add - docker_registry_issuer / {{ docker_registry_issuer }}
- Add - docker_registry_issuer_password / {{ docker_registry_issuer_password }}
- Add - deployhost / {{ MACHINENAME }}.{{ server_base_domain }}
- Add - deployuser / gitlab-runner
- Add - BASE_DOMAIN / {{ server_base_domain }}

DON'T set the variables to protected - or they are not available in branches