#!/bin/bash

export MACHINENAME="shell"
export SERVERTYPE="cx11"

if [ -z "$HETZNER_API_TOKEN" ]; then 
    echo "HETZNER_API_TOKEN not set" >&2
    exit -1
fi

if ! [ -x "$(command -v docker)" ]; then
  echo 'Error: docker is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v docker-machine)" ]; then
  echo 'Error: docker-machine is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v docker-machine-driver-hetzner)" ]; then
  echo 'Error: docker-machine-driver-hetzner is not installed. - check this  https://github.com/JonasProgrammer/docker-machine-driver-hetzner' >&2 
  exit 1
fi

if ! [ -x "$(command -v tar)" ]; then
  echo 'Error: tar is not installed.' >&2
  exit 1
fi

if [ ! -f $MACHINENAME/.realvars.yml ]; then
    echo "error: $MACHINENAME/.realvars.yml not found"
    echo "    please copy $MACHINENAME/vars.yml to $MACHINENAME/.realvars.yml and modify content"
    echo "    (the .realvars.yml is in gitignore)"
    exit -1
fi

if [ $(hostname) == "ci" ]; then
  echo 'Error: Dont run this programm on the ci host!' >&2
  exit 1
fi

echo "creating installation package"
rm -rf tmp
mkdir -p tmp
cp -r $MACHINENAME tmp
cd tmp
mv $MACHINENAME/.realvars.yml $MACHINENAME/vars.yml
tar czf $MACHINENAME.tgz $MACHINENAME
cd ..

echo "creating machine"
docker-machine create \
  --engine-storage-driver overlay2 \
  --driver hetzner \
  --hetzner-server-type $SERVERTYPE  \
  --hetzner-image debian-9 \
  $MACHINENAME

echo "moving package to server"
docker-machine scp tmp/$MACHINENAME.tgz $MACHINENAME:/root
rm -rf tmp

echo "assimilating server"
docker-machine ssh $MACHINENAME "cd /root && tar xvf $MACHINENAME.tgz && cd $MACHINENAME && ./install-machine.sh \"$HETZNER_API_TOKEN\" \"$MACHINENAME\" && cd .. && rm -rf $MACHINENAME*"

echo "done"