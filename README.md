# Creates a Shell host + Gitlab CI/CD host from the Shellhost

## Hetzner Cloud API Docs

<https://docs.hetzner.cloud/#top>

## Ansible Tutorial Links

<https://www.youtube.com/watch?v=uut4hdc9aXc>

<https://www.youtube.com/watch?v=vFPOv3zn7eY>

<https://github.com/johanan/Dockerized-Wordpress-the-Correct-way>

## How to create passwords for Users that are created with ansible

<http://docs.ansible.com/ansible/latest/reference_appendices/faq.html#how-do-i-generate-crypted-passwords-for-the-user-module>

$ debian:
apt-get install whois
mkpasswd --method=sha-512

$OSX / python:
pip install passlib
python -c "from passlib.hash import sha512_crypt; import getpass; print(sha512_crypt.using(rounds=5000).hash(getpass.getpass()))"

## Create users in ansible from a list of users

<https://github.com/msergiy87/ansible-create-users/>


## Some twriks are required to have FreeIPA behind our LetsEncrypt companion Ngix

<https://www.adelton.com/freeipa/freeipa-behind-ssl-proxy>


## Talking to the FreeIPA API via curl

find out the REST Api of FreeIPA

docker exec -it ipa /bin/bash

      kinit admin
      ipa -vv user-show admin --raw
      ipa -vv group-add --desc='AD users1' ad_users1  --raw

<https://vda.li/en/posts/2015/05/28/talking-to-freeipa-api-with-sessions/>

<http://www.admin-magazine.com/Archive/2016/34/A-REST-interface-for-FreeIPA>

<https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/identity_management_guide/managing-users>

<https://bugzilla.redhat.com/show_bug.cgi?id=1061865>

## Vim & debilian

<https://unix.stackexchange.com/questions/318824/vim-cutpaste-not-working-in-stretch-debian-9>

## Lots about GitLab

<https://docs.gitlab.com/ee/administration/auth/ldap.html>
<https://cstan.io/?p=7923>
<https://about.gitlab.com/handbook/support/workflows/support-engineering/ldap/debugging_ldap.html>
 <https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1891>
email - <https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1079>

This features only work on EE
  group_base: 'cn=groups,cn=accounts,dc=foo,dc=com'
  admin_group: 'gitlab_admin'  
  
## So do this by the UI
- (Login with LDAP Account of the user if the user is not in the Usertab)
- Admin->Overview->Users->"user name"->Edit->Access->Access Level -> set to Admin  

## This is not supported since omnibus-gitlab 7.9.0. The settings were moved to Admin->Settings->Sign-up restrictions->Sign-up enabled (to false) inside of GitLab.
  gitlab_rails['signup_enabled'] = false;
  gitlab_rails['signin_enabled'] = false;
  gitlab_rails['gitlab_signup_enabled'] = false;
  gitlab_rails['gitlab_signin_enabled'] = false;

## email system

<https://docs.gitlab.com/omnibus/settings/smtp.html>

<https://docs.gitlab.com/omnibus/settings/smtp.html#testing-the-smtp-configuration>


## get API token

echo "print Gitlab::CurrentSettings.current_application_settings.runners_registration_token" | gitlab-rails console -e production

SHARED_RUNNER_TOKEN=$(echo "print Gitlab::CurrentSettings.current_application_settings.runners_registration_token" | gitlab-rails console -e production | tail -2 | head -1 | sed -e 's/nil$//')
echo TOKEN: $SHARED_RUNNER_TOKEN

with docker

SHARED_RUNNER_TOKEN=$(docker exec -it gitlab /bin/bash -c "echo 'print Gitlab::CurrentSettings.current_application_settings.runners_registration_token' | gitlab-rails console -e production | tail -2 | head -1 | sed -e 's/nil$//'")

echo TOKEN: $SHARED_RUNNER_TOKEN


## get gitlab token for adding gloabal keys

https://gitlab.com/gitlab-org/gitlab-ce/issues/27954

docker exec -ti -u git gitlab gitlab-rails r "token = PersonalAccessToken.new(user: User.where(id: 1).first, name: 'root', token: 'miimimimi', scopes: ['api','read_user','sudo','read_repository','read_registry']); token.save"'!'

keys: https://gitlab.com/gitlab-org/gitlab-ce/issues/36285


# add deploy key

curl --request POST --header "PRIVATE-TOKEN: 9koXpg98eAheJpvBs5tK" --header "Content-Type: application/json" --data '{"title": "My deploy key", "key": "ssh-rsa AAAA...", "can_push": "true"}' "https://gitlab.example.com/api/v4/projects/5/deploy_keys/"

#gitlab registry

https://github.com/sameersbn/docker-gitlab/blob/master/docs/container_registry.md

  registry_nginx['listen_port'] = 5000 
  registry_nginx['listen_https'] = false
  registry_nginx['proxy_set_headers'] = {
    "X-Forwarded-Proto" => "https",
    "X-Forwarded-Ssl" => "on"
  }

  https://mtarnawa.org/2017/11/10/running-secured-private-docker-registry-nginx-proxy-letsencrypt/